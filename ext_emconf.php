<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "df_tabs".
 *
 * Auto generated 28-06-2013 18:35
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['df_tabs'] = [
	'title' => 'Tabbed Content and Pages',
	'description' => 'Create tab based content elements and pages easily and flexible with configurable mouseover handling, animations and autoplay features! It requires jquery.',
	'category' => 'fe',
	'version' => '9.1.3',
	'state' => 'stable',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => 'sgalinski Internet Services',
	'constraints' => [
		'depends' => [
			'typo3' => '12.4.0-12.4.99',
			'php' => '8.1.0-8.3.99',
		],
		'conflicts' => [
		],
		'suggests' => [
		],
	],
];
