<?php

use SGalinski\DfTabs\Controller\PluginController;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(
	static function () {
		ExtensionManagementUtility::addPItoST43(
			'df_tabs',
			'',
			'_plugin1',
			'list_type',
			TRUE
		);

		// we correct the classname of the plugin controller
		ExtensionManagementUtility::addTypoScriptSetup(
			'plugin.tx_dftabs_plugin1.userFunc=' . PluginController::class . '->main'
		);

		ExtensionManagementUtility::addPageTSConfig(
			'@import "EXT:df_tabs/Configuration/TsConfig/Page/NewContentElementWizard.tsconfig"'
		);
	}
);
