<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\DfTabs\Controller;

use SGalinski\DfTabs\DataProvider\AbstractBaseDataProvider;
use SGalinski\DfTabs\Domain\Repository\TabRepository;
use SGalinski\DfTabs\Service\ConfigurationService;
use SGalinski\DfTabs\View\FluidView;
use TYPO3\CMS\Core\Localization\LocalizationFactory;
use TYPO3\CMS\Core\TypoScript\TypoScriptService;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MathUtility;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Plugin for the 'df_tabs' extension
 */
class PluginController {
	/**
	 * @var string
	 */
	public string $prefixId = 'tx_dftabs_plugin1';

	/**
	 * @var string
	 */
	public string $scriptRelPath = 'Resources/Private/Language/locallang.xlf';

	/**
	 * @var string
	 */
	public string $extKey = 'df_tabs';

	/**
	 * @var array
	 */
	public array $conf = [];

	/**
	 * @var bool
	 */
	public bool $LOCAL_LANG_loaded = FALSE;

	/**
	 * @var string
	 */
	public string $LLkey = 'default';

	/**
	 * @var array
	 */
	public array $LOCAL_LANG = [];

	/**
	 * @var array
	 */
	protected array $LOCAL_LANG_UNSET = [];

	/**
	 * @var string
	 */
	public string $altLLkey = '';

	/**
	 * This is the incoming array by name $this->prefixId merged between POST and GET, POST taking precedence.
	 * Eg. if the class name is 'tx_myext'
	 * then the content of this array will be whatever comes into &tx_myext[...]=...
	 *
	 * @var array
	 */
	public array $piVars = [
		'pointer' => '',
		// Used as a pointer for lists
		'mode' => '',
		// List mode
		'sword' => '',
		// Search word
		'sort' => '',
	];

	/**
	 * @var ContentObjectRenderer
	 */
	protected ContentObjectRenderer $cObj;

	/**
	 * Data Provider Instance
	 *
	 * @var AbstractBaseDataProvider
	 */
	protected AbstractBaseDataProvider $dataProvider;

	/**
	 * Plugin configuration
	 *
	 * @var array
	 */
	protected array $pluginConfiguration = [];

	/**
	 * Returns an instance of the renderer
	 *
	 * @param string $tabId
	 * @return FluidView
	 */
	protected function getRenderer(string $tabId): FluidView {
		/** @var FluidView $renderer */
		$renderer = GeneralUtility::makeInstance(FluidView::class, $this->pluginConfiguration);
		return $renderer;
	}

	/**
	 * Returns an instance of the configuration manager
	 *
	 * @return ConfigurationService
	 */
	protected function getConfigurationManager(): ConfigurationService {
		/** @var ConfigurationService $configurationManager */
		$configurationManager = GeneralUtility::makeInstance(ConfigurationService::class);
		$configurationManager->injectPluginController($this);
		return $configurationManager;
	}

	/**
	 * Returns an instance of the tab repository
	 *
	 * @return TabRepository
	 */
	protected function getTabRepository(): TabRepository {
		/** @var TabRepository $repository */
		$repository = GeneralUtility::makeInstance(TabRepository::class);
		$repository->injectContentObject($this->cObj);
		$repository->injectPluginConfiguration($this->pluginConfiguration);

		return $repository;
	}

	/**
	 * Controls the data flow from the repository to the view to render
	 * the tab menus
	 *
	 * @param string $content
	 * @param array $configuration
	 * @return string
	 */
	public function main(string $content, array $configuration): string {
		$tabId = sha1(microtime());

		try {
			$this->conf = $configuration;
			$this->pi_setPiVarDefaults();
			$this->pi_loadLL();
			$this->pi_initPIflexForm();

			$this->pluginConfiguration = $this->getConfigurationManager()->getConfiguration();

			$repository = $this->getTabRepository();
			$renderer = $this->getRenderer($tabId);

			$records = $repository->getRecords();
			$tabElements = $repository->buildTabElements(
				$this->pluginConfiguration['titles'] ?? [],
				$records
			);
			$content .= $renderer->renderTabs($tabElements, $tabId);
		} catch (\Exception $exception) {
			$content = $exception->getMessage();
		}

		if (isset($this->pluginConfiguration['classPrefix'])) {
			$this->prefixId = $this->pluginConfiguration['classPrefix'] . 'plugin1';
		} else {
			$this->prefixId = 'plugin1';
		}

		return $this->pi_wrapInBaseClass($content);
	}

	/**
	 * Wraps the input string in a <div> tag with the class attribute set to the prefixId.
	 * All content returned from your plugins should be returned through this function so all content from your plugin is encapsulated in a <div>-tag nicely identifying the content of your plugin.
	 *
	 * @param string $str HTML content to wrap in the div-tags with the "main class" of the plugin
	 * @return string HTML content wrapped, ready to return to the parent object.
	 */
	public function pi_wrapInBaseClass(string $str): string {
		$content = '<div class="' . str_replace('_', '-', $this->prefixId) . '">
		' . $str . '
	</div>
	';
		if (!($this->frontendController->config['config']['disablePrefixComment'] ?? FALSE)) {
			$content = '


	<!--

		BEGIN: Content of extension "' . $this->extKey . '", plugin "' . $this->prefixId . '"

	-->
	' . $content . '
	<!-- END: Content of extension "' . $this->extKey . '", plugin "' . $this->prefixId . '" -->

	';
		}

		return $content;
	}

	/**
	 * Converts $this->cObj->data['pi_flexform'] from XML string to flexForm array.
	 *
	 * @param string $field Field name to convert
	 */
	public function pi_initPIflexForm(string $field = 'pi_flexform'): void {
		// Converting flexform data into array
		$fieldData = $this->cObj->data[$field] ?? NULL;
		if (!is_array($fieldData) && $fieldData) {
			$this->cObj->data[$field] = GeneralUtility::xml2array((string) $fieldData);
			if (!is_array($this->cObj->data[$field])) {
				$this->cObj->data[$field] = [];
			}
		}
	}

	/**
	 * Loads local-language values from the file passed as a parameter or
	 * by looking for a "locallang" file in the
	 * plugin class directory ($this->scriptRelPath).
	 * Also locallang values set in the TypoScript property "_LOCAL_LANG" are
	 * merged onto the values found in the "locallang" file.
	 * Supported file extensions xlf
	 *
	 * @param string $languageFilePath path to the plugin language file in format EXT:....
	 */
	public function pi_loadLL(string $languageFilePath = ''): void {
		if ($this->LOCAL_LANG_loaded) {
			return;
		}

		if ($languageFilePath === '' && $this->scriptRelPath) {
			$languageFilePath = 'EXT:' . $this->extKey . '/' . PathUtility::dirname(
				$this->scriptRelPath
			) . '/locallang.xlf';
		}

		if ($languageFilePath !== '') {
			$languageFactory = GeneralUtility::makeInstance(LocalizationFactory::class);
			$this->LOCAL_LANG = $languageFactory->getParsedData($languageFilePath, $this->LLkey);
			$alternativeLanguageKeys = GeneralUtility::trimExplode(',', $this->altLLkey, TRUE);
			foreach ($alternativeLanguageKeys as $languageKey) {
				$tempLL = $languageFactory->getParsedData($languageFilePath, $languageKey);
				if ($this->LLkey !== 'default' && isset($tempLL[$languageKey])) {
					$this->LOCAL_LANG[$languageKey] = $tempLL[$languageKey];
				}
			}

			// Overlaying labels from TypoScript (including fictitious language keys for non-system languages!):
			if (isset($this->conf['_LOCAL_LANG.'])) {
				// Clear the "unset memory"
				$this->LOCAL_LANG_UNSET = [];
				foreach ($this->conf['_LOCAL_LANG.'] as $languageKey => $languageArray) {
					// Remove the dot after the language key
					$languageKey = substr($languageKey, 0, -1);
					// Don't process label if the language is not loaded
					if (is_array($languageArray) && isset($this->LOCAL_LANG[$languageKey])) {
						foreach ($languageArray as $labelKey => $labelValue) {
							if (!is_array($labelValue)) {
								$this->LOCAL_LANG[$languageKey][$labelKey][0]['target'] = $labelValue;
								if ($labelValue === '') {
									$this->LOCAL_LANG_UNSET[$languageKey][$labelKey] = '';
								}
							}
						}
					}
				}
			}
		}

		$this->LOCAL_LANG_loaded = TRUE;
	}

	/**
	 * If internal TypoScript property "_DEFAULT_PI_VARS." is set then it will merge the current $this->piVars array onto these default values.
	 */
	public function pi_setPiVarDefaults(): void {
		if (isset($this->conf['_DEFAULT_PI_VARS.']) && is_array($this->conf['_DEFAULT_PI_VARS.'])) {
			$this->conf['_DEFAULT_PI_VARS.'] = $this->applyStdWrapRecursive($this->conf['_DEFAULT_PI_VARS.']);
			$typoScriptService = GeneralUtility::makeInstance(TypoScriptService::class);
			$tmp = $typoScriptService->convertTypoScriptArrayToPlainArray($this->conf['_DEFAULT_PI_VARS.']);
			ArrayUtility::mergeRecursiveWithOverrule($tmp, is_array($this->piVars) ? $this->piVars : []);
			$tmp = $this->removeInternalNodeValue($tmp);
			$this->piVars = $tmp;
		}
	}

	/**
	 * Remove the internal array key _typoScriptNodeValue
	 */
	protected function removeInternalNodeValue(array $typoscript): array {
		foreach ($typoscript as $key => $value) {
			if ($key === '_typoScriptNodeValue') {
				unset($typoscript[$key]);
			}

			if (is_array($value)) {
				$typoscript[$key] = $this->removeInternalNodeValue($value);
			}
		}

		return $typoscript;
	}

	/**
	 * Recursively looks for stdWrap and executes it
	 *
	 * @param array $conf Current section of configuration to work on
	 * @param int $level Current level being processed (currently just for tracking; no limit enforced)
	 * @return array Current section of configuration after stdWrap applied
	 */
	protected function applyStdWrapRecursive(array $conf, int $level = 0): array {
		foreach ($conf as $key => $confNextLevel) {
			if (str_contains($key, '.')) {
				$key = substr($key, 0, -1);

				// descend into all non-stdWrap-subelements first
				foreach ($confNextLevel as $subKey => $subConfNextLevel) {
					if (is_array($subConfNextLevel) && str_contains($subKey, '.') && $subKey !== 'stdWrap.') {
						$conf[$key . '.'] = $this->applyStdWrapRecursive($confNextLevel, $level + 1);
					}
				}

				// now for stdWrap
				foreach ($confNextLevel as $subKey => $subConfNextLevel) {
					if (is_array($subConfNextLevel) && $subKey === 'stdWrap.') {
						$conf[$key] = $this->cObj->stdWrap($conf[$key] ?? '', $conf[$key . '.']['stdWrap.'] ?? []);
						unset($conf[$key . '.']['stdWrap.']);
						if (empty($conf[$key . '.'])) {
							unset($conf[$key . '.']);
						}
					}
				}
			}
		}

		return $conf;
	}

	/**
	 * Return value from somewhere inside a FlexForm structure
	 *
	 * @param array $T3FlexForm_array FlexForm data
	 * @param string $fieldName Field name to extract. Can be given like "test/el/2/test/el/field_templateObject" where each part will dig a level deeper in the FlexForm data.
	 * @param string $sheet Sheet pointer, eg. "sDEF
	 * @param string $lang Language pointer, eg. "lDEF
	 * @param string $value Value pointer, eg. "vDEF
	 * @return string|null The content.
	 */
	public function pi_getFFvalue(
		array $T3FlexForm_array,
		string $fieldName,
		string $sheet = 'sDEF',
		string $lang = 'lDEF',
		string $value = 'vDEF'
	) {
		$sheetArray = $T3FlexForm_array['data'][$sheet][$lang] ?? '';
		if (is_array($sheetArray)) {
			return $this->pi_getFFvalueFromSheetArray($sheetArray, explode('/', $fieldName), $value);
		}

		return NULL;
	}

	/**
	 * Returns part of $sheetArray pointed to by the keys in $fieldNameArray
	 *
	 * @param array $sheetArray Multidimensional array, typically FlexForm contents
	 * @param array $fieldNameArr Array where each value points to a key in the FlexForms content - the input array will have the value returned pointed to by these keys. All integer keys will not take their integer counterparts, but rather traverse the current position in the array and return element number X (whether this is right behavior is not settled yet...)
	 * @param string $value Value for outermost key, typ. "vDEF" depending on language.
	 * @return mixed The value, typ. string.
	 * @see pi_getFFvalue()
	 */
	public function pi_getFFvalueFromSheetArray(array $sheetArray, array $fieldNameArr, string $value) {
		$tempArr = $sheetArray;
		foreach ($fieldNameArr as $k => $v) {
			if (MathUtility::canBeInterpretedAsInteger($v)) {
				if (is_array($tempArr)) {
					$c = 0;
					foreach ($tempArr as $values) {
						if ($c == $v) {
							$tempArr = $values;
							break;
						}

						$c++;
					}
				}
			} elseif (isset($tempArr[$v])) {
				$tempArr = $tempArr[$v];
			}
		}

		return $tempArr[$value] ?? '';
	}

	public function setContentObjectRenderer(ContentObjectRenderer $cObj): void {
		$this->cObj = $cObj;
	}

	public function getContentObjectRenderer(): ContentObjectRenderer {
		return $this->cObj;
	}

	public function getConf(): array {
		return $this->conf;
	}

	public function setConf(array $conf): PluginController {
		$this->conf = $conf;
		return $this;
	}
}
