<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\DfTabs\EventListeners;

use SGalinski\DfTabs\Preview\PreviewService;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;

/**
 * This event listener cares for the rendering of the plugin preview
 */
class PageContentPreviewRenderingEventListener {
	/**
	 * @var PreviewService
	 */
	protected PreviewService $previewService;

	public function __construct(PreviewService $previewService) {
		$this->previewService = $previewService;
	}

	public function __invoke(PageContentPreviewRenderingEvent $event): void {
		if ($event->getTable() === 'tt_content') {
			$record = $event->getRecord();
			if ($record['list_type'] === 'df_tabs_plugin1') {
				$event->setPreviewContent(
					$this->previewService->getPluginView($event->getRecord())->render('Tabs')
				);
			}
		}
	}
}
