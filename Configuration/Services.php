<?php

use SGalinski\DfTabs\EventListeners\PageContentPreviewRenderingEventListener;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use TYPO3\CMS\Backend\View\Event\PageContentPreviewRenderingEvent;

return static function (ContainerConfigurator $containerConfigurator): void {
	$services = $containerConfigurator->services();
	$services->defaults()
		->private()
		->autowire()
		->autoconfigure();

	$services->load('SGalinski\\DfTabs\\', __DIR__ . '/../Classes/');

	$services->set(PageContentPreviewRenderingEventListener::class)
		->tag(
			'event.listener',
			['event' => PageContentPreviewRenderingEvent::class]
		);
};
