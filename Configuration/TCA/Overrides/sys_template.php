<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

// add static typoscript template
ExtensionManagementUtility::addStaticFile(
	'df_tabs',
	'Configuration/TypoScript/',
	'df_tabs'
);
