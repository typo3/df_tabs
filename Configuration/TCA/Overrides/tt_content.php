<?php

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

// add plugin
ExtensionManagementUtility::addPlugin(
	[
		'LLL:EXT:df_tabs/Resources/Private/Language/locallang.xlf:tt_content.list_type_plugin1',
		'df_tabs_plugin1',
		'extension-df_tabs-content-element'
	],
	'list_type',
	'df_tabs'
);

// remove default plugin fields
$list = 'select_key,pages,recursive';
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['df_tabs_plugin1'] = $list;

// add flexform configuration
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['df_tabs_plugin1'] = 'pi_flexform';
ExtensionManagementUtility::addPiFlexFormValue(
	'df_tabs_plugin1',
	'FILE:EXT:df_tabs/Configuration/FlexForms/flexform.xml'
);
