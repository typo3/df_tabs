<?php

use TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider;

/**
 * Important! Do not return a variable named $icons, because it will result in an error.
 * The core requires this file and then the variable names will clash.
 * Either use a closure here, or do not call your variable $icons.
 */
return [
	'extension-df_tabs-content-element' => [
		'provider' => BitmapIconProvider::class,
		'source' => 'EXT:df_tabs/Resources/Public/Icons/contentElementWizard.png'
	]
];
