# Version 9 Breaking Changes

- Dropped TYPO3 10 and 11 support
- Dropped PHP 7 support

# Version 8 Breaking Changes

- Dropped TYPO3 9 support
- removed deprecated TypoScript Renderer
- updated base css, in case you import the css by default, check your implementation
- removed _alternative and _flat css

# Version 7 Breaking Changes

- Dropped TYPO3 8 support
